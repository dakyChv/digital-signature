const crypto = require("crypto");
const axios = require("axios");

async function testSign(keyId, keyPair) {
  // Prepare the parameters.
  //
  const msg = `{"greeting": "hello world!"}`;
  // Signature parameters
  const algorithm = "ecdsa-hmacsha256";
  const created = Math.floor(Date.now() / 1000);
  const expires = Math.floor(Date.now() / 1000) + 9000000;
  const headers = "(request-target) (created) (expires) digest";

  const hmsg = crypto.createHash("sha256").update(msg).digest();
  const digest64 = Buffer.from(hmsg).toString("base64");
  const digest256 = `SHA-256=${digest64}`;

  const toSign = `(request-target): post /auth/v1/signature\n(created): ${created}\n(expires): ${expires}\ndigest: ${digest256}`;
  const signer = crypto.createSign("SHA256");
  signer.update(toSign);
  const signValue = signer.sign(keyPair.privateKey);
  const sign64 = signValue.toString("base64");

  // Config http headers.
  const config = {
    headers: {
      signature: `keyId="${keyId}", algorithm="${algorithm}", created=${created}, expires=${expires}, headers="${headers}", signature="${sign64}"`,
    },
  };

  // Auth with digital signature.
  await axios
    .post("http://localhost:8083/auth/v1/signature", msg, config)
    .then((res) => {
      const headerDate =
        res.headers && res.headers.date ? res.headers.date : "no response date";
      console.log("Status Code:", res.status);
      console.log("Date in Response header:", headerDate);

      const data = res.data;
      console.log(data);
    })
    .catch((err) => {
      console.log("Error: ", err.message);
    });
}

async function execute() {
  // Generate a new ECDSA key pair using P-256 curve
  const keyPair = crypto.generateKeyPairSync("ec", {
    namedCurve: "P-256",
  });

  const pub = keyPair.publicKey.export({
    type: "spki",
    format: "der",
  });

  const pubKey64 = Buffer.from(pub, "utf-8").toString("base64");

  // Handshake
  await axios
    .post("http://localhost:8083/auth/v1/handshake", {
      terminalId: "TD7216788",
      publicKey: pubKey64,
    })
    .then((res) => {
      const headerDate =
        res.headers && res.headers.date ? res.headers.date : "no response date";
      console.log("Status Code:", res.status);
      console.log("Date in Response header:", headerDate);

      const data = res.data;

      console.log("=== KeyID: " + data.keyId + "\n\n\n");

      // Test signature
      //
      testSign(data.keyId, keyPair);
    })
    .catch((err) => {
      console.log("Error: ", err.message);
    });
}

execute();
